#!/usr/bin/env bash

# change KDE
lookandfeeltool -a org.kde.breeze.desktop

# Firefox changes automatically with KDE

# change emacs config.  Only takes effect for new emacs instances.
sed -i -e "s/load-theme 'solarized-gruvbox-dark/load-theme 'solarized-light/g" ~/.emacs.d/init.el

# change Konsole.  Only takes effect for new Konsoles.
sed -i -e 's/ColorScheme=Solarized$/ColorScheme=SolarizedLight/g' ~/.local/share/konsole/Joel\'s\ Konsole\ Profile.profile

# change wallpaper
wallpaper.bash

ssh tessera "irsend SEND_ONCE ls0964 on"
ssh tessera "irsend SEND_ONCE ls0964 white"

