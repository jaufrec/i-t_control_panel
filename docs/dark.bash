#!/usr/bin/env bash

# change KDE
lookandfeeltool -a org.kde.breezedark.desktop
# TODO: fix the theme issue so that changing themes doesn't change
# the task switcher.
# https://userbase.kde.org/Plasma/How_to_create_a_Unity-like_look_and_feel_theme_using_Plasma%E2%80%99s_Desktop_Scripting_API
# https://develop.kde.org/docs/extend/plasma/theme/quickstart/


# Firefox changes automatically with KDE

# change emacs config.  Only takes effect for new emacs instances.
sed -i -e "s/load-theme 'solarized-light/load-theme 'solarized-gruvbox-dark/g" ~/.emacs.d/init.el

# change Konsole.  Only takes effect for new Konsoles.
sed -i -e 's/ColorScheme=SolarizedLight$/ColorScheme=Solarized/g' ~/.local/share/konsole/Joel\'s\ Konsole\ Profile.profile

# change wallpaper
wallpaper.bash dark

ssh tessera "irsend SEND_ONCE ls0964 on"
ssh tessera "irsend SEND_ONCE ls0964 red"
