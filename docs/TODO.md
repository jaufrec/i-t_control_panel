* ~~power cmd uses config file~~
* ~~mqtt observer uses config file~~
* ~~handle toggle, on, off by default~~
* ~~all three clients work~~
* spotify pause and next work
* all linting passes (except pylint, due to module import configuration that works for all the other linters)
* test cases
* encryption
* client subscriptions should be added via on_connect, not willy-nilly, so that they automatically reconnect.
