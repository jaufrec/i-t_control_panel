# Installation notes

## Hardware



## Notes on installing a cluster of these from local build.

### Configure Pip to install from a local respository
```~/.pip/pip.conf
[global]

; Extra index to private pypi dependencies
extra-index-url = http://192.168.1.4/pypi/
trusted-host = 192.168.1.4```

then pip install idiotcontrol


### Installing as systemd service

1. create the file.

```/etc/systemd/system/buttonbox.service```

```[Unit]
Description=Send MQTT messages when buttons are pressed
After=multi-user.target

[Service]
Type=simple
Restart=always
User=pi
WorkingDirectory=/home/pi
ExecStart=/home/pi/.local/bin/idiotcontrol /home/pi/i🥴t.toml  publisher 

[Install]
WantedBy=multi-user.target```

2. sudo systemctl daemon-reload
3. sudo systemctl start buttonbox.service
4. sudo systemctl enable buttonbox.service

# development notes

## notes on fake gpio for testing

run_power:  ##           run the program in power subsciber mode
	cd src && export GPIO_PIN_FACTORY=mock && python -m idiotcontrol.__main__ ../i🥴t.toml subscriber
