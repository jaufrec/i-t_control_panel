# -*- coding: utf-8 -*-
""" Command-line tool to send MQTT messages to IdiOT Control subscribers, using
IdiOT Control configuration. """


import json
import logging

import paho.mqtt.client as mqtt_client

from idiotcontrol.config import parsing
from idiotcontrol.config.errors import ConfigError
from idiotcontrol.config.mqtt import config_connect

logger = logging.getLogger(__name__)


def main():
    """Parse configuration file to get mqtt settings, and
    send the specified message
    :raises ConfigError: if the Topic isn't one of the allowed ones
"""

    parser = parsing.argparser(
        "send_message to the mqtt queue for all IdiOT subscribers"
    )
    parser.add_argument(
        "topic",
        type=str,
        nargs=1,
        help="Topic to control.  Must be present in config file",
    )
    parser.add_argument(
        "payload",
        type=str,
        nargs=1,
        help="Payload to send.  Must be present in config file",
    )
    args = parser.parse_args()
    topic: str = args.topic[0]
    payload: str = args.payload[0]
    parsing.configure_logging(args.verbose)
    (mqtt_dict, config_dict) = parsing.load(args.config_file)
    mode: str = parsing.SUBSCRIBER
    subscribers: list = parsing.parse(config_dict, mode)
    topics: set = {x.topic for x in subscribers}  # use a set to remove duplicates
    if topic not in topics:
        raise ConfigError(
            f"Topic must be one of {', '.join(topics)}, but it is {topic}."
        )
    client: mqtt_client.Client() = mqtt_client.Client()
    config_connect(client, mqtt_dict)
    client.publish(topic, json.dumps(payload))
    logger.debug(f"Published {topic} with {payload}")


if __name__ == "__main__":
    main()
