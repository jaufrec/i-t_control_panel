# -*- coding: utf-8 -*-

""" Master file for running buttonbox MQTT publisher and subscriber.
A device with eight LED buttons, controlled by an MCP23017
Pins 0 through 7: momentary buttons.
pins 8 through 15: LEDs

Numbered

     0     2     4    6
     8    10    12   14

     1     3     5    7
     9    11    13   15

    Status (one-at-time test)
8:    yes
9:    lights 9, 14
10:   lights 10, 13, 14
11:   lights 11, 13, 14
12:   lights 12, 13, 14
13:   no
14:   no
15:   lights up 13?, 14

"""

# pylint: disable=consider-iterating-dictionary,consider-using-dict-items,wrong-import-position,wrong-import-order

import json
import logging
import time
from typing import Dict
from threading import Thread

from adafruit_mcp230xx.mcp23017 import MCP23017
import board
import busio
import digitalio
import paho.mqtt.client as mqtt

logging.basicConfig(
    datefmt="%Y-%m-%d %H:%M:%S %z",
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)

mcp: MCP23017 = MCP23017(busio.I2C(board.SCL, board.SDA))
gpio_device: Dict[int, digitalio.DigitalInOut] = {}
for pin_num in range(0, 8):  # publisher pins
    gpio_device[pin_num] = mcp.get_pin(pin_num)
    gpio_device[pin_num].switch_to_input(pull=digitalio.Pull.UP)
for pin_num in range(8, 16):  # subscriber pins
    gpio_device[pin_num] = mcp.get_pin(pin_num)
    gpio_device[pin_num].switch_to_output(value=False)
sub_controls: dict = {}
pub_controls: dict = {}


def on_connect(mclient: mqtt.Client, _userdata, _flags, _rc) -> None:
    """Announce existence on mqtt and subscribe to topics.  Subscribing
    in this callback ensures we will re-subscribe with every reconnection.
    :param mclient: Client() object"""
    logger.info("ButtonBox connected to MQTT")
    mclient.publish("announce", "ButtonBox connected")
    for topic in sub_controls.keys():
        logger.info(f"Listens to {topic}")
        mclient.subscribe(topic, qos=2)


def on_message(_client: str, _userdata: str, msg: mqtt.MQTTMessageInfo) -> None:  # type: ignore
    """Accept a message. Parse the payload as JSON, validate it,
    and change a GPIO pin accordingly.
    :param msg: message to receive as JSONb"""
    topic: str = msg.topic
    logger.info(f"Received message {msg.payload} on topic {topic}")
    if not hasattr(msg, "payload"):
        logger.info("  Invalid Message")
        return
    try:
        control = sub_controls.get(topic)
        if not control:
            logger.warning(f"Recevied msg for {topic} but not subscribed.")
            return
        payload: str = json.loads(msg.payload.decode("UTF-8"))
        if not isinstance(payload, str):
            logger.warning("Payload not a string.")
            return
        logger.debug(f"  ­ Payload: {payload}")
        payload_dict: dict = control.get("payload")
        if not payload_dict or not isinstance(payload_dict, dict):
            logger.warning(f"This program is not configured to handle any payload on {topic}.")
            return
        payload_translation: bool = payload_dict.get(payload, None)
        if not isinstance(payload_translation, bool):
            logger.warning(
                f"Received {payload} for {topic} but this program"
                + " is not configured for that specific payload."
            )
            return
        control["device"].value = payload_translation
    except (json.JSONDecodeError, TypeError) as E:
        logger.warning(f"Error reading message: {E.__class__}: {E}")
    except Exception as E:  # pylint: disable=broad-except
        logger.error(
            f"Unexpected error reading or executing {msg}: ({E.__class__}, {E}"
        )


def pin_poll_loop(mclient: mqtt.Client) -> None:
    """ Wrapper for threaded looping """
    while True:
        poll_pins(mclient)
        time.sleep(0.1)


def poll_pins(mclient: mqtt.Client) -> None:
    """ Poll all pins
    Run this in a thread so it's not blocking anything else, like MQTT listening """
    for poll_pin in pub_controls.keys():
        # reversing polarity on input switches because they function as open=True
        pin_value: bool = not gpio_device[poll_pin].value
        if pin_value:  # should only be true while button is temporarily pressed
            control: dict = pub_controls[poll_pin]
            topic: str = control['topic']
            # in order to toggle, we need to know the current state and send the reverse
            led_pin = sub_controls[topic]['device']
            current_state: bool = led_pin.value
            try:
                message: str = control['toggle_message'][current_state]
            except KeyError:
                logger.warning(f"  – Don't know what to do if {topic} is {current_state}")
                break
            mclient.publish(topic, payload=json.dumps(message), retain=True)
            led_pin.value = not current_state
            logger.info(f"published {topic}: {json.dumps(message)}")
            time.sleep(0.5)  # brute force button cooldown; unfortunately, it cools down all the buttons


def main():
    """Main execution for IdiOT ButtonBox MQTT service.
    Set up MQTT and GPIO actions.  Poll GPIO on a perpetual loop,
    accepting MQTT interrupts at all times.
    """
    mclient: mqtt.Client = mqtt.Client()
    logger.info("I🥴t Control Panel System starting for ButtonBox.")

    sub_controls["lamp"] = {"device": gpio_device[8], "payload": {"on": True, "off": False}}
    sub_controls["monitors"] = {"device": gpio_device[15], "payload": {"on": True, "off": False}}
    sub_controls["videolights"] = {"device": gpio_device[9], "payload": {"on": True, "off": False}}
    sub_controls["night_mode"] = {"device": gpio_device[12], "payload": {"night": True, "day": False}}
    pub_controls[0] = {"topic": "lamp", "toggle_message": {True: "off", False: "on"}}
    pub_controls[7] = {"topic": "monitors", "toggle_message": {True: "off", False: "on"}}
    pub_controls[1] = {"topic": "videolights", "toggle_message": {True: "off", False: "on"}}
    pub_controls[5] = {"topic": "night_mode", "toggle_message": {"day": "night", "night": "day",
                                                                 False: "night", True: "day"}}
    mclient.on_connect = on_connect
    mclient.on_message = on_message

    try:
        mclient.connect("192.168.1.96", 1883, 60)
        logger.info("Connected ButtonBox to MQTT")
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E

    # use threading to share between pub and sub
    # Method 1: run pin polling in a python thread and run mqtt loop_forever
    # works fine.

    pin_poll_loop_thread = Thread(target=pin_poll_loop, args=(mclient,))
    pin_poll_loop_thread.start()
    mclient.loop_forever()

    # method 2: paho mqtt background thread mode (loop_start)
    # using paho mqtt loop to share between pub and sub
    # works fine.
    # mclient.loop_start()
    # while True:
    #    poll_pins(mclient)
    #    time.sleep(0.5)

    # method 3: thread both gpio and mqtt listeners
    # too fast/concurrent.  bounces.

    # mclient.loop_start()
    # pin_poll_loop_thread = Thread(target=pin_poll_loop, args=(mclient,))
    # pin_poll_loop_thread.start()

    logger.info("Disconnected ButtonBox from MQTT")

# if __name__ == "__main__":
#     import ipdb, traceback, sys  # noqa DEBUG ONLY pylint: disable=multiple-imports

#     try:  # DEBUG ONLY
#         main()
#     except:  # noqa pylint: disable=bare-except
#         extype, value, tb = sys.exc_info()
#         traceback.print_exc()
#         ipdb.post_mortem(tb)


if __name__ == "__main__":
    main()
