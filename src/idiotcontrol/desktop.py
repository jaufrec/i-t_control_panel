# -*- coding: utf-8 -*-

""" Master file for running desktop MQTT listener as part of i🥴t network.

"""

# pylint: disable=consider-iterating-dictionary,consider-using-dict-items,wrong-import-position,wrong-import-order

import json
import logging
import os
import subprocess
from typing import Callable

import paho.mqtt.client as mqtt

logging.basicConfig(
    datefmt="%Y-%m-%d %H:%M:%S %z",
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)
sub_controls: dict = {}


def _local_xshell(command: str) -> None:
    """Run a local command that will modify X."""
    my_env = os.environ.copy()
    my_env["DISPLAY"] = ":0"
    subprocess.run([command], env=my_env, check=True)


def local_light() -> None:
    """Run a bash script that hopefully does everything necessary to turn
    on local light mode.
    """
    _local_xshell("/usr/local/bin/light.bash")


def local_dark() -> None:
    """Run a bash script that hopefully does everything necessary to turn
    off local light mode."""
    _local_xshell("/usr/local/bin/dark.bash")


def on_connect(mclient: mqtt.Client, _userdata, _flags, _rc) -> None:
    """Announce existence on mqtt and subscribe to topics.  Subscribing
    in this callback ensures we will re-subscribe with every reconnection.
    :param mclient: Client() object"""
    logger.info("ButtonBox connected to MQTT")
    mclient.publish("announce", "ButtonBox connected")
    for topic in sub_controls.keys():
        logger.info(f"Listens to {topic}")
        mclient.subscribe(topic, qos=2)


def on_message(_client: str, _userdata: str, msg: mqtt.MQTTMessageInfo) -> None:  # type: ignore
    """Accept a message. Parse the payload as JSON, validate it,
    run shell commands accordingly.
    :param msg: message to receive as JSONb"""
    topic: str = msg.topic
    logger.info(f"Received message {msg.payload} on topic {topic}")
    if not hasattr(msg, "payload"):
        logger.info("  Invalid Message")
        return
    try:
        control = sub_controls.get(topic)
        if not control:
            logger.warning(f"Recevied msg for {topic} but not subscribed.")
            return
        payload: str = json.loads(msg.payload.decode("UTF-8"))
        if not isinstance(payload, str):
            logger.warning("Payload not a string.")
            return
        logger.debug(f"  ­ Payload: {payload}")
        payload_dict: dict = control.get("payload")
        if not payload_dict or not isinstance(payload_dict, dict):
            logger.warning(f"This program is not configured to handle any payload on {topic}.")
            return
        shell_function: Callable = payload_dict.get(payload, None)
        if not callable(shell_function):
            logger.warning(
                f"Received {payload} for {topic} but this program"
                + " does not have a valid function for that specific payload."
            )
            return
        shell_function()
    except (json.JSONDecodeError, TypeError) as E:
        logger.warning(f"Error reading message: {E.__class__}: {E}")
    except Exception as E:  # pylint: disable=broad-except
        logger.error(
            f"Unexpected error reading or executing {msg}: ({E.__class__}, {E}"
        )


def main():
    """Main execution for IdiOT ButtonBox MQTT service.
    Set up MQTT and listen forever.  on_message triggers shell commands.
    """
    mclient: mqtt.Client = mqtt.Client()
    logger.info("I🥴t Control Panel System starting for ButtonBox.")

    sub_controls["night_mode"] = {"payload": {"night": local_dark, "day": local_light,
                                              "on": local_dark, "off": local_light}}
    mclient.on_connect = on_connect
    mclient.on_message = on_message

    try:
        mclient.connect("192.168.1.96", 1883, 60)
        logger.info("Connected Desktop to MQTT")
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E

    mclient.loop_forever()
    logger.info("Disconnected ButtonBox from MQTT")


if __name__ == "__main__":
    main()
