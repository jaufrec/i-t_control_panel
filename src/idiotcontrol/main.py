# -*- coding: utf-8 -*-

"""Master file for Internet of Things Control Panel package, which
can be run in control-panel-publisher, power-subscriber, or
desktop-subscriber modes.

"""

import json
import logging
import sys
import paho.mqtt.client as mqtt_client

from idiotcontrol import __version__ # reads from __init__.py
from idiotcontrol.config.parsing import argparser, IdiOTConfig
from idiotcontrol.config.mqtt import config_connect
from idiotcontrol.hosts import publisher, subscriber

logger = logging.getLogger(__name__)

def main():
    """Main execution for all flavors of IdiOT server.  Parse arguments, load
    configuration, and start the appropriate mode"""

    parser = parsing.argparser()
    args = parser.parse_args()
    level: int = logging.WARNING
    if args.verbose:
        level = logging.INFO
        if args.verbose > 1:
            level = logging.DEBUG

    logging.basicConfig(
        level=level,
        format="%(asctime)s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %z",
    )

    config = parsing.load(args.config_file)
    mode: str = args.mode
    host: str = mode  # use as default
    if args.host:
        host = args.host  # override if specified
    confs: list = parsing.parse(config_dict, mode, host)
    client: mqtt_client.Client() = mqtt_client.Client()

    def on_subs_connect(client, userdata, _flags, _rc) -> None:
        """Announce and subscribe"""
        client.publish("announce", json.dumps({"mode": mode, "host": host}))
        for topic in userdata:
            logger.debug(f"DEBUG scope stuff: {topic}")
            client.subscribe(topic)

    def on_pub_connect(client, _userdata, _flags, _rc) -> None:
        """Announce."""
        client.publish("announce", "Control Box activated")

    logger.info(f"I🥴t Control Panel System starting {mode} mode for {host}")
    
    if mode == parsing.PUBLISHER:
        publisher.main(client, confs)  # TODO: for debugging, capture the return value
        client.on_connect = on_pub_connect
    elif mode == parsing.SUBSCRIBER:
        results: dict = subscriber.main(confs)
        topic_list = results.get("topic_list", [])
        on_message = results.get("on_message")
        client.user_data_set(topic_list)
        client.on_connect = on_subs_connect
        client.on_message = on_message

    client = config_connect(client, mqtt_dict)

    # for debugging publisher:
    # locals()["device_instances"][1].pin.drive_low()
    # locals()["device_instances"][1].pin.drive_high()

    client.loop_forever()
    logger.info("Stopping i🥴t Control Panel System")


if __name__ == "__main__":
    import ipdb, traceback, sys  # noqa DEBUG ONLY pylint: disable=multiple-imports

    try:  # DEBUG ONLY
        main()
    except:  # noqa pylint: disable=bare-except
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        ipdb.post_mortem(tb)

# if __name__ == "__main__":
    main()
