# -*- coding: utf-8 -*-
""" Command-line debugging tool to monitor all mqtt traffic, using IdiOT Control configuration
to connect. """

import logging

import paho.mqtt.client as mqtt_client

logger = logging.getLogger(__name__)


def on_connect(client, _userdata, _flags, _rc):
    """Subscribe to everything
    :param client: etc
    """
    client.subscribe("#")


def on_message(_client, _userdata, msg):
    """Print everything to stdout
    :param msg: etc
    """
    print(msg.topic + " " + str(msg.payload))


def main():
    """Parse configuration file to get mqtt settings, and
    subscribe"""

    mclient: mqtt_client.Client = mqtt_client.Client()
    mclient.on_connect = on_connect
    mclient.on_message = on_message
    try:
        mclient.connect("192.168.1.96", 1883, 60)
        logger.info("Connected MQTT_watch to MQTT")
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E
    mclient.loop_forever()
    logger.info("Disconnected MQTT_watch from MQTT")


import ipdb
import sys
import traceback

try:
    main()
except:  # pylint: disbale=bare-except
    extype, value, tb = sys.exc_info()
    traceback.print_exc()
    ipdb.post_mortem(tb)
