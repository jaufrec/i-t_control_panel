# -*- coding: utf-8 -*-
""" Provide functions for local execution on an idiotcontrol host """

import logging
import os
import subprocess
import time

logger = logging.getLogger(__name__)


def _suspend() -> None:
    """Suspend the host.  Needs to be a method that works without root access."""
    subprocess.run(["systemctl", "suspend"], check=True)


def _local_xshell(command: str) -> None:
    """Run a local command that will modify X."""
    my_env = os.environ.copy()
    my_env["DISPLAY"] = ":0"
    subprocess.run([command], env=my_env, check=True)


def local_light() -> None:
    """Run a bash script that hopefully does everything necessary to turn
    on local light mode.
    """
    _local_xshell("light.bash")


def local_dark() -> None:
    """Run a bash script that hopefully does everything necessary to turn
    off local light mode."""
    _local_xshell("dark.bash")


def next_music() -> None:
    """Skip to the next track on all accessible music players"""
    logger.debug("Next Music")
    raise NotImplementedError


def _volume_change(quantity: str) -> None:
    """Base command to change local system volume"""
    subprocess.run(["amixer", "-D", "pulse", "set", "Master", quantity], check=True)


def volume_up() -> None:
    """Change local volume up 5%."""
    _volume_change("5%+")


def volume_down() -> None:
    """Change local volume down 5%."""
    _volume_change("5%-")


def just_pause() -> None:
    """Toggle the play state of the music player(s)"""
    raise NotImplementedError


def playpause() -> None:
    """Toggle the play state of the music player(s)"""
    raise NotImplementedError


def sleep() -> None:
    """Run commands necessary to put the host to sleep, i.e., suspend"""
    time.sleep(10)  # leave time for any other shutdown commands to hit
    _suspend()
