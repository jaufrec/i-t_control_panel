# -*- coding: utf-8 -*-

"""Server functionality for Subscriber, a host which listens for mqtt
messages and takes local actions, which may be shell commands or
activating GPIO-controlled power relays.

"""

import json
import logging
from typing import List

from idiotcontrol.config.parsing import SubscriberConf
from idiotcontrol.config.errors import InvalidMessage

logger = logging.getLogger(__name__)


outlets: list = []
control_inst: dict = {}

def control_setup(confs: List[SubscriberConf]) -> dict:
    """Given all of the subscriber configurations, return a dict of dicts
    containing lists of all of the actions (bound methods), indexed by topic
    and keyword.

    :param confs: list of Confs that apply to this host
    :raises Exception: for configuration problems
    """
    for conf in confs:
        conf.setup_device()
        if not control_inst.get(conf.topic):
            control_inst[conf.topic] = {}

        # if there are multiple actions configured, condense
        # them down to a single list

        for keyword in conf.payload.keys():
            if not control_inst[conf.topic].get(keyword):
                control_inst[conf.topic][keyword] = []
            try:
                # this turns {'on': 'on'} into {'on': digitaloutputdevice(1).on}
                action = getattr(conf.device, conf.payload[keyword])
            except Exception as E:
                raise Exception(f"error configuring subscriber {conf}: {E}") from E
            control_inst[conf.topic][keyword] += [action]
    return control_inst


def execute_payload(topic: str, payload: str) -> None:
    """Accept a payload, and try to run the value as a function on the object named by the key

    :param topic: The MQTT topic to listen to
    :param payload: The MQTT content to listen to
"""
    if control_inst[topic].get(payload):
        try:
            for item in control_inst[topic][payload]:
                logger.debug(f"Executing {item}")
                item()
        except KeyError as E:
            logger.error(
                f"Received {payload} for {topic} but {topic}"
                + f" is not configured for that payload.  {E}"
            )  # TODO: reduce this catch, which is too broad
        except Exception as E:
            logger.error(f"Unexpected error doing {payload}: {E.__class__}: {E}")


def on_message(client: str, userdata: str, msg: dict) -> None:  # type: ignore
    """Accept a message. Parse the payload as JSON and—if it's a string tha matches
    the list of allowed strings for this topic, do it.

    :param client: Why is this param here if we aren't using it?
    :param userdata: Why is this here?
    :param msg: message to send as JSON
    :raises InvalidMessage: if the message doesn't have the expected contents
"""
    logger.debug(msg)
    if not hasattr(msg, "payload"):
        raise InvalidMessage
    try:
        payload: str = json.loads(msg["payload"].decode("UTF-8"))
        if isinstance(payload, str) and control_inst[msg["payload"]].get(payload):
            execute_payload(msg["topic"], payload)
    except (json.JSONDecodeError, TypeError) as E:
        logger.debug(f"Error reading message: {E.__class__}: {E}")
    except Exception as E:
        logger.debug(f"Unexpected error reading message: ({E.__class__}, {E}")


def main(control_confs: List[SubscriberConf]) -> dict:
    """Activate GPIO devices, set up event handlers for all configured mqtt messages,
    and listen to mqtt forever."""

    global control_inst  # to make them available to the on_message execution scope
    result: dict = {}
    topic_list: list = []
    control_inst = control_setup(control_confs)
    for control in control_confs:
        topic_list += [control.topic]
    result["on_message"] = on_message
    result["topic_list"] = topic_list
    return result
