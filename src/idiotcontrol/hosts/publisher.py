# -*- coding: utf-8 -*-

"""Server functionality for idiotcontrol Publisher, a host which
accepts GPIO signals and publishes messages to mqtt server
"""

import logging
from typing import List

import gpiozero
import paho.mqtt.client as mqtt_client
from gpiozero.input_devices import DigitalInputDevice

from idiotcontrol.config.parsing import PublisherConf

logger = logging.getLogger(__name__)


def control_setup(
    confs: List[PublisherConf], client: mqtt_client.Client
) -> List[DigitalInputDevice]:
    """Given a list of control configurations objects, instantiate each
    control, and assign its when_* functions.  We need to pass the
    client to the function that the event handler calls, but the event
    handler only passes on one thing, the device instance. The trick
    here is to assign the thing we want to pass through the event
    handler to the device _class_ so that it gets set as a default
    value on the device instance—the one thing the event handler will
    pass on when triggered.  In other words, by assigning the client
    to a new member of device Class object, before the device is
    instantiated from it, the client will be a default value of the
    class and therefore passed on to the instantiation, which in turn
    will be passed on to the event handler when the event happens.
    See
    https://gpiozero.readthedocs.io/en/stable/faq.html#why-do-i-get-an-attributeerror-trying-to-set-attributes-on-a-device-object
    The 'control' is a configuration class, the 'device' is a gpiozero
    class, and the device_inst is an instantiation of the device with
    the information from the control.  The event handlers are
    given functions by reference (e.g., control.do_act), _not_ the
    results of function _calls_ (e.g. control.do_act()).

    """

    device_instances: List[DigitalInputDevice] = []
    try:
        for conf in confs:
            device = conf.device
            device.client = client
            if device == gpiozero.Button:
                device_inst = device(pin=conf.pin, bounce_time=conf.bounce_time)  # type: ignore
                device_inst.when_pressed = conf.on_action
                device_inst.when_released = conf.off_action
            elif device == gpiozero.RotaryEncoder:
                device_inst = device(
                    a=conf.pin,
                    b=conf.second_pin,
                    max_steps=conf.max_steps,
                    bounce_time=conf.bounce_time,
                )  # type: ignore
                device_inst.when_rotated_clockwise = conf.on_action
                device_inst.when_rotated_counter_clockwise = conf.off_action
            logger.debug(f"{conf.explain()}")
            logger.info(f"enabled publisher on {conf.topic}")
            device_instances += [device_inst]  # type: ignore
    except gpiozero.exc.BadPinFactory as E:
        raise Exception(
            "Probably this device doesn't have GPIO.  Or, it does, "
            + f"but a python package like RPi.gpio, is missing.  Error: {E}"
        ) from E
    return device_instances


def main(
    client: mqtt_client.Client, control_confs: List[PublisherConf]
) -> List[DigitalInputDevice]:
    """Activate GPIO devices with event handlers that send mqtt messages,
    and wait for mqtt forever, which keeps mqtt alive while waiting for GPIO
    events forever."""
    return control_setup(control_confs, client)
