# -*- coding: utf-8 -*-
""" Custom errors with extra information for idiotcontrol """


class ConfigError(Exception):
    """Indicator of problems in configuration file"""


class InvalidMessage(Exception):
    """A message is missing something required.  Message should have payload."""
