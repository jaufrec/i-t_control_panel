# -*- coding: utf-8 -*-
""" Connect to mqtt client """

import paho.mqtt.client as mqtt_client  # type: ignore

from idiotcontrol.config.errors import ConfigError


def config_connect(client: mqtt_client.Client, mqtt_info: dict) -> mqtt_client.Client:
    """Parse mqtt information, connect using the provided client object,
    and return the now-connected client object"""
    try:
        client.connect(mqtt_info["server"], mqtt_info["port"], 60)
    except KeyError as E:
        raise ConfigError(
            f"mqtt connection information in config file must be valid: {E}"
        ) from E
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E
    return client
