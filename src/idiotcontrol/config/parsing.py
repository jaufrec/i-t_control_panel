# -*- coding: utf-8 -*-
""" Data classes and methods for parsing CLI input """


import argparse
import json
import logging
from dataclasses import dataclass, field
from typing import Any, List, Optional

import paho.mqtt.client as mqtt_client
import toml
from gpiozero import (
    Button,
    DigitalInputDevice,
    DigitalOutputDevice,
    GPIOPinInUse,
    RotaryEncoder,
)

from idiotcontrol.__init__ import __version__
from idiotcontrol.config.errors import ConfigError
from idiotcontrol.hosts import shell_command

logger = logging.getLogger(__name__)

PUBLISHER = "publisher"
SUBSCRIBER = "subscriber"
VALID_MODES = [PUBLISHER, SUBSCRIBER]


def argparser(description: Optional[str] = "IdiOT Control Panel") -> argparse.ArgumentParser:
    """Return an argparser with a few shared arguments"""
    parser = argparse.ArgumentParser(
        f"{description}, Version {__version__}"
    )
    parser.add_argument("config_file", help="path to controls configuration TOML file.")
    parser.add_argument(
        "mode", help="Mode of program to operate.", choices=VALID_MODES
    )
    parser.add_argument(
        "-v", "--verbose", help="Show additional logging.", action="store_true"
    )
    parser.add_argument("--host", help="Specify a host to filter out all other config entries.")

    return parser


def configure_logging(debug: bool):
    """Return a preconfigured logger object"""

    level: int = logging.INFO
    if debug:
        level = logging.DEBUG

    logging.basicConfig(
        level=level,
        format="%(asctime)s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %z",
    )


class GPIOBoard():
    """ Each object is one physical GPIO-enabled device, with numbered GPIO pins."""

    mcpdevice = False

    def __init__(self, *args, **kwargs):
        """ If the device is an Adafruit I2C bonnet with extra GPIO pins,
        initialize it and pre-load the pin pool.  Otherwise, do nothing."""
        self.pinpool = {}        
        try:
            import board
            import busio
            from adafruit_mcp230xx.mcp23017 import MCP23017
            mcp: MCP23017 = MCP23017(busio.I2C(board.SCL, board.SDA))
            self.mcpdevice = True
            self.pinpool: dict = {}
            for pin_num in range(0,16):
                self.pinpool[pin_num] = mcp.get_pin(pin_num)
        except ModuleNotFoundError:
            pass
        super().__init__(*args, **kwargs)


# reuse gpio pins so we don't try to reinitialize a pin already in use
gpiodev: GPIOBoard = GPIOBoard()

# get the list of valid shell methods for validation
shell_methods = [
    name for name in dir(shell_command) if callable(getattr(shell_command, name))
]


@dataclass
class HostConf:
    """Base class for idiot hosts"""

    host: str
    topic: str
    pin: Optional[int] = None


@dataclass
class PublisherConf(HostConf):
    """config for idiot hosts that publish mqtt messages"""

    on_msg: Any = None
    off_msg: Any = None
    device: Any = None
    device_type: str = "button"
    bounce_time: Any = None
    second_pin: int = 0  # for rotary encoder
    max_steps: int = 0  # for rotary encoder

    def explain(self) -> str:
        """Provide a plain language explanation of the device rules"""
        result: str = ""
        if self.device_type == "button":
            if self.on_msg:
                result += f"When GPIO pin {self.pin} changes from off to on,"
                result += f" send '{self.on_msg}' to topic '{self.topic}'.\n"
            if self.off_msg:
                result += f"When GPIO pin {self.pin} changes from on to off,"
                result += f" send '{self.off_msg}' to topic '{self.topic}'.\n"
        else:
            result += (
                f"When rotary encoder with pins {self.pin} and {self.second_pin} … "
            )
            if self.on_msg:
                result += f" … rotates clockwise, send '{self.on_msg}' to topic '{self.topic}'"
            if self.off_msg:
                result += f" … rotates counter-clockwise, send topic '{self.off_msg}'"
                result += f" to '{self.topic}'"
            result += ".\n"
        return result

    def __post_init__(self) -> None:
        if self.device_type == "button":
            self.device = Button
        elif self.device_type == "rotary":
            self.device = RotaryEncoder
            if self.second_pin == 0:
                raise ConfigError(
                    f"If device_type is rotary ({self.topic}), provide second_pin."
                )
        else:
            raise ConfigError(
                "Each host must be configured with either no device_type,"
                + f"or 'button' or 'rotary'.  not {self.device_type}"
            )

    def send_messages(self, message: str, device: DigitalInputDevice) -> None:
        """Send an mqtt message based on the this conf and depending on how triggered"""
        client: mqtt_client = device.client  # type: ignore
        client.publish(self.topic, json.dumps(message))  # type: ignore

    def on_action(self, device: DigitalInputDevice) -> None:
        """method for off event handling"""
        if self.on_msg:
            self.send_messages(self.on_msg, device)

    def off_action(self, device: DigitalInputDevice) -> None:
        """method for off event handling"""
        if self.off_msg:
            self.send_messages(self.off_msg, device)


@dataclass
class SubscriberConf(HostConf):
    """config for idiot hosts that subscribe to mqtt messages."""

    payload: dict = field(default_factory=dict)
    device: Any = None

    global gpiodev

    def explain(self) -> str:
        """Provide a plain language explanation of the device rules"""
        result: str = f"Listen to mqtt for topic '{self.topic}'.\n"
        for key, value in self.payload.items():
            result += f"On hearing message '{key}', do '{self.device}.{value}'.\n"
        return result

    def setup_device(self) -> None:
        """If a pin is present, set up a DigitalOutputDevice.  Otherwise, assume
        it's a shell command and validate it."""
        if isinstance(self.pin, int):
            try:
                subject_pin = gpiodev.pinpool[self.pin]
                if subject_pin:
                    subject_pin.switch_to_output(value=True)
                else:
                    subject_pin = DigitalOutputDevice(self.pin, active_high=False)
            except GPIOPinInUse:
                # assume it's already set up, and set up as an output device  # TODO test that
                pass
            try:
                self.device = subject_pin
            except Exception as E:
                raise ConfigError(
                    "problem setting up Subcriber.  Check that the pin number is valid. "
                    + f"Error: {E}"
                ) from E
            return

        # assume it's a shell action, then.
        for action in self.payload.values():  # validate all the shell commands
            if action in shell_methods:
                self.device = shell_command
            # else:
            # TODO; figure out how to specific whether a shell command is expected or not
            # and if yes, then add put this back:
            #     raise ConfigError(
            #         f"Shell command '{action}' must be in '{', '.join(shell_methods)}' to be valid."
            #     )



def get_publishers(
    topic: str, topic_body: dict, host: Optional[str] = ""
) -> List[PublisherConf]:
    """Return all publishers for this topic that match the host, as Conf objects."""

    pubs = topic_body.get(PUBLISHER, [])
    # if there's only publisher in the toml entry, then it will show up as a dict instead of
    # a list of dicts.  Make it consistent here:
    if isinstance(pubs, dict):
        pubs = [pubs]

    publisher_entries: list = []
    for pub in pubs:
        if not pub.get("host"):
            pub["host"] = PUBLISHER
        if not pub.get("on_msg") and not pub.get("off_msg"):
            pub["on_msg"] = "on"
            pub["off_msg"] = "off"
        publisher_entries += [pub]

    if host:
        pubs = [pub for pub in publisher_entries if pub["host"] == host]

    try:
        publisher_objects: List[PublisherConf] = [
            PublisherConf(topic=topic, **pub) for pub in pubs
        ]
    except TypeError as E:
        logger.warning(
            f"Problem setting up publishers for {topic}.  Check configuration file. {E}"
        )
    return publisher_objects


def get_subscribers(
    topic: str , topic_body: dict, host: Optional[str] = ""
) -> List[SubscriberConf]:
    """Return all subscribers for this topic that match the host, as Conf objects."""
    subs = topic_body.get(SUBSCRIBER, [])
    # See get_publishers for explanation of this:
    if isinstance(subs, dict):
        subs = [subs]

    subscriber_entries: list = []
    for sub in subs:
        if not sub.get("host"):
            sub["host"] = SUBSCRIBER
        payload = sub.get("payload")
        if not isinstance(payload, dict):
            sub["payload"] = {"on": "on", "off": "off", "toggle": "toggle"}
        subscriber_entries += [sub]

    if host:
        subs = [sub for sub in subscriber_entries if sub["host"] == host]

    try:
        subscriber_objects: List[SubscriberConf] = [
            SubscriberConf(topic=topic, **sub) for sub in subs
        ]
    except TypeError as E:
        logger.warning(
            f"Problem setting up subscribers for {topic}.  Check configuration file. {E}"
        )
    return subscriber_objects


def load(config_file: str) -> tuple:
    """Load and validate the configuration file.
    Return two dicts, one for the mqtt client info and one for the control topics.
    Each entry in the toml file must follow the taxonomy:

    1st level: i🥴t
    2nd level: topic (matches the mqtt topic)
    3rd level: host type, one of VALID_MODES

    One topic must be 'mqtt' and hold the server and port variables.

"""
    try:
        config_dict: dict = toml.load(config_file)["i🥴t"]
    except toml.TomlDecodeError as E:
        raise ConfigError(
            f"Use a valid TOML format for the config file.  Error: {E}"
        ) from E
    except Exception as E:
        raise Exception(f"Use a valid configuration file at {config_file}") from E

    # Separate the mqtt info (removing the mqtt key from the dict, so
    # it won't be mistaken for a topic)

    try:
        mqtt_dict: Any = config_dict.pop(
            "mqtt"
        )
        if not isinstance(mqtt_dict, dict):
            raise ConfigError()
    except (KeyError, ConfigError) as E:
        raise Exception("Configuration file needs a valid 'mqtt' section") from E

    topics = config_dict.keys()
    if not topics:
        raise ConfigError("The configuration file must have at least one topic")
    host_type: set = set()
    for topic in topics:
        for key in config_dict[topic].keys():
            host_type.add(key)
    bad_types = host_type - set(VALID_MODES)
    if bad_types:
        raise ConfigError(
            f"All host types must be in {VALID_MODES}.  Found: {bad_types}"
        )
    return (mqtt_dict, config_dict)


def parse(config_dict: dict, mode: str, host: Optional[str] = None) -> list:
    """Given a validated configuration dictionary, return a list of publishers
    or subscribers, as indicated. filter by host, if provided"""
    if mode == PUBLISHER:
        publishers: List[PublisherConf] = []
        for topic in config_dict.keys():
            publishers += get_publishers(topic, config_dict[topic], host)
        return publishers
    if mode == SUBSCRIBER:
        subscribers: List[SubscriberConf] = []
        for topic in config_dict.keys():
            subscribers += get_subscribers(topic, config_dict[topic], host)
        return subscribers
    raise ConfigError(f"Mode must be in {VALID_MODES} but it is {mode}")
