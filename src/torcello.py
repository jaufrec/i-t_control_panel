# -*- coding: utf-8 -*-
"""Template for Internet of Things device daemons."""

import json
import logging
import socket
import sys
from paho.mqtt import client as mqtt_client
from gpiozero import (
    Button,
    DigitalInputDevice,
    DigitalOutputDevice,
    RotaryEncoder,
)

SERVER: str = "192.168.1.89"
PORT: int = 1883
ROLE: str = "Template"
HOST: str = socket.gethostname()
OUTPINS: dict = {25: "25"}  # dict of gpio pin # to mqtt topic
TOPIC_MAP: dict = {value: key for key, value in OUTPINS.items()}
DEVICE: dict = {}
YESSY: list = ["yes", "y", "on", 1, True, "t", "true"]
NOEY: list = ["no", "n", "off", 0, False, "f", "false"]
logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %z",
)
logging.getLogger("asyncio").setLevel(logging.WARNING)


def main():
    """Main loop for all flavors of IdiOT server.

    Raises:
        Exception if mqtt doesn't connect.

    """

    for pin_num in OUTPINS.keys():
        DEVICE[pin_num] = DigitalOutputDevice(pin_num, active_high=False)

    def on_connect(client, userdata, flags, reason_code, properties):
        topics: list = list(OUTPINS.values())
        message = json.dumps({"mode": "subscriber", "host": HOST, "topics": topics})
        logger.info(f"Connected with result code {reason_code}.  announcing: {message}")
        client.publish("announce", message)
        for topic in topics:
            client.subscribe(HOST + "/" + topic)

    def on_message(client, userdata, message):
        logger.debug(
            f"client={client}, userdata={userdata}, message={message.topic}, payload={message.payload}"
        )
        if not message.payload or not message.topic:
            # assume we are loading the program, and don't freak out.
            return
        try:
            data: list = str(message.topic).split("/")
            data.remove(HOST)
            sub_topic = data[0]
            if len(data) != 1 or sub_topic not in OUTPINS.values():
                raise Exception(f"topic {message.topic} is not a valid subtopic for {HOST}")
            pin = TOPIC_MAP[sub_topic]
            payload = str(json.loads(message.payload)).lower()
            if payload in YESSY:
                DEVICE[pin].on()
            elif payload in NOEY:
                DEVICE[pin].off()
            else:
                raise Exception(
                    f"Message {message} had Unrecognized payload of {payload}"
                )
        except Exception as E:
            logger.error(f"Unexpected error reading message: {E}")

    client: mqtt_client.Client() = mqtt_client.Client(
        mqtt_client.CallbackAPIVersion.VERSION2
    )
    try:
        client.connect(SERVER, PORT, 60)
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E

    client.enable_logger()
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_forever()

    logger.info(f"Stopping {ROLE}")


if __name__ == "__main__":
    # import ipdb, traceback, sys  # noqa DEBUG ONLY pylint: disable=multiple-imports

    try:  # DEBUG ONLY
        main()
    except:  # noqa pylint: disable=bare-except
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        ipdb.post_mortem(tb)

    # if __name__ == "__main__":
    main()
