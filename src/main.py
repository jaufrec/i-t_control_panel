# -*- coding: utf-8 -*-
"""Template for Internet of Things device daemons."""

import json
import logging
import socket
import sys
from paho.mqtt import client as mqtt_client
from gpiozero import (
    Button,
    DigitalInputDevice,
    DigitalOutputDevice,
    RotaryEncoder,
)

SERVER: str = "192.168.1.89"
PORT: int = 1883
ROLE: str = "Template"
HOST: str = socket.gethostname()
OUTPINS: dict = {25: "25"}   # dict of gpio pin # to mqtt topic
DEVICE: dict = {}

logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %z",
)
logging.getLogger('asyncio').setLevel(logging.WARNING)

def main():
    """Main loop for all flavors of IdiOT server.

    Raises:
        Exception if mqtt doesn't connect.

"""

    for pin_num in OUTPINS.keys():
        DEVICE[pin_num] = DigitalOutputDevice(pin_num, active_high=False)
        DEVICE[pin_num].switch_to_output(value=True)
                                              
    def on_connect(client, userdata, flags, reason_code, properties):
        topics: list = list(OUTPINS.values())
        message = json.dumps({"mode": "subscriber", "host": HOST, "topics": topics})
        logger.info(f"Connected with result code {reason_code}.  announcing: {message}")
        client.publish("announce", message)
        for topic in topics:
            client.subscribe(HOST + "/" + topic)

    def on_message(client, userdata, message):
        logger.debug(f"client={client}, userdata={userdata}, message={message}")
        try:
            payload: str = json.loads(msg["payload"].decode("UTF-8"))
        except Exception as E:
            logger.error(f"Unexpected error reading message: {E}")
        if payload:
            logger.debug(f"payload: {payload}")
        # do the pin thing

    client: mqtt_client.Client() = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION2)
    try:
        client.connect(SERVER, PORT, 60)
    except Exception as E:
        raise Exception(f"Problem starting the mqtt server: {E}") from E

    client.enable_logger()
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_forever()

    logger.info(f"Stopping {ROLE}")


if __name__ == "__main__":
    import ipdb, traceback, sys  # noqa DEBUG ONLY pylint: disable=multiple-imports

    try:  # DEBUG ONLY
        main()
    except:  # noqa pylint: disable=bare-except
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        ipdb.post_mortem(tb)

# if __name__ == "__main__":
    main()
